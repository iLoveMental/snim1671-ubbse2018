FROM node:6
RUN mkdir app1
WORKDIR app1
COPY package.json /app1
RUN npm cache clean
RUN npm install
COPY . /app1
EXPOSE 3000
CMD ["npm","start"]