const mongoose = require('mongoose')

// alkatreszek sema
const ComponentSema = mongoose.Schema({

  marka: {
    type: String,
    required: true
  },

  tipus: {
    type: String,
    required: true
  },

  ar: {
    type: Number,
    required: true
  },

  model: {
    type: String,
    required: true
  },

  teljesitmeny: {
    type: String,
    required: true
  }

})

const Components = module.exports = mongoose.model('Components', ComponentSema)

module.exports.listComponents = function (callback) {
  Components.find({}, callback)
}

module.exports.addComponent = function (newComponent, callback) {
  newComponent.save(callback)
}
