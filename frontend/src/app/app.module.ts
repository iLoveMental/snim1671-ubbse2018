import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';

import { ComponentsService } from './services/components.service';
import { ListComponent } from './components/list/list.component';
import { AddComponent } from './components/add/add.component';
import { ValidateService } from './services/validate.service';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { RegLoginService } from './services/reglogin.service';
import { RegLoginGuard } from './guards/reglogin.guard';


//meagadjuk az utakat bizonyos oldalakhoz
const appRoutes = [
{path: 'login', component : LoginComponent},
{path: 'register', component:RegisterComponent},
{path: 'home', component:HomeComponent},
{path: 'list', component:ListComponent},
{path: 'add', component:AddComponent, canActivate: [RegLoginGuard]}
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    HomeComponent,
    ListComponent,
    AddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot()
  ],
  providers: [ValidateService,RegLoginService, RegLoginGuard, ComponentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
