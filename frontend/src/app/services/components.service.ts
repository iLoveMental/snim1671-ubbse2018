import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})
export class ComponentsService {
  regToken: any;
  felhasznalo: any;
  constructor(private http: Http) { }

  //elkuldjuk a  felhasznalo tokenjet es visszakapjuk a szervertol az alkatreszeket
    componentsQuery(){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.get('http://localhost:3000/users/list', {headers:headers}).map(res => res.json());
  }

  addComponent(newComponent){
    let headers = new Headers();
    this.tokenBetolt();
    headers.append('Authorization', this.regToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/add', newComponent, {headers:headers}).map(res => res.json());
  }

  //betoltjuk a tokent
    tokenBetolt(){
      const token = localStorage.getItem('id_token');
      this.regToken = token;
    }
}
