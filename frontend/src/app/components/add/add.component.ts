import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ComponentsService } from '../../services/components.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
    marka: String;
    tipus : String;
    model : String;
    ar : Number;
    teljesitmeny : String;

  constructor(
    private  flashMessage : FlashMessagesService,
    private  componentsService : ComponentsService,
    private  router : Router
  ) { }

  ngOnInit() {

  }

  hozzaadas(){
    const newComponent={
      marka  : this.marka,
      tipus  : this.tipus,
      model  : this.model,
      teljesitmeny  : this.teljesitmeny,
      ar : this.ar
    }

    this.componentsService.addComponent(newComponent).subscribe(data =>{

      if(data.succes){
        this.flashMessage.show(data.msg, {cssClass : 'alert-success', timeout: 3500});
        this.router.navigate(['/home']);
      }
      else{
        this.flashMessage.show(data.msg , {cssClass : 'alert-danger', timeout: 3500});
        this.router.navigate(['/add']);
      }
    });
  }

}
