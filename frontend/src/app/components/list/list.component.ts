import { Component, OnInit } from '@angular/core';
import { ComponentsService } from '../../services/components.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  components:Object;

  constructor(
    private componentsService : ComponentsService,
    private router : Router
  ) { }

//tehat mikor betoltjuk a profil oldalt akkor le kell kerjuk a felhasznalo adataid
  ngOnInit() {
    this.componentsService.componentsQuery().subscribe(list => {
    this.components = list.components;

  }, err =>{
      console.log(err);
      return false;
  });
  }

}
