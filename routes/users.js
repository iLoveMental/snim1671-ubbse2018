const express = require('express')
const router = express.Router()
const passport = require('passport')
const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Component = require('../models/components')
const config = require('../config/database')

// regisztracio
router.post('/register', (req, res, next) => {
  let ujFelhasznalo = new User({
    nev: req.body.nev,
    felhasznalonev: req.body.felhasznalonev,
    email: req.body.email,
    jelszo: req.body.jelszo,
    admin: 0
  })
  User.addUser(ujFelhasznalo, (err) => {
    if (err) res.json({ succes: false, msg: 'Unsuccesfull registration!!!' })
    else res.json({ succes: true, msg: 'Succesfull registration!!!' })
  })
})

// login
router.post('/login', (req, res, next) => {
  const felhasznalonev = req.body.felhasznalonev
  const jelszo = req.body.jelszo

  User.getUserByUsername(felhasznalonev, (err, felhasznalo) => {
    if (err) throw err
    // ha nem letezik a felahasznalo akkor uzenetet kuldunk vissz ellenben checkoljuk a jelszavat
    if (!felhasznalo) return res.json({ succes: false, msg: 'Wrong username!!!' })

    User.jelszoEllenorzes(jelszo, felhasznalo.jelszo, (err, ok) => {
      if (err) throw err
      if (ok) {
        const token = jwt.sign({ data: felhasznalo }, config.secret, {
          expiresIn: 3600 // 1 ora mulva lejar a session
        })
        res.json({
          succes: true,
          token: 'JWT ' + token,
          felhasznalo: {
            id: felhasznalo._id,
            nev: felhasznalo.nev,
            felhasznalonev: felhasznalo.felhasznalonev,
            email: felhasznalo.email,
            admin: felhasznalo.admin
          }
        })
      } else {
        return res.json({ succes: false, msg: 'Wrong password!!!' })
      }
    })
  })
})

// listazas
router.get('/list', (req, res, next) => {
  Component.listComponents((err, result) => {
    if (err) this.flashMessage.show('Unsuccesfull listing!!!', { cssClass: 'alert-danger', timeout: 3500 })
    else res.json({ components: result })
  })
})

// hozzadas
router.post('/add', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  let newComponent = new Component({
    marka: req.body.marka,
    tipus: req.body.tipus,
    model: req.body.model,
    teljesitmeny: req.body.teljesitmeny,
    ar: req.body.ar
  })
  Component.addComponent(newComponent, (err) => {
    if (err) res.json({ succes: false, msg: 'Unsuccesfull addition!!!' + err })
    else res.json({ succes: true, msg: 'Succesfull addition!!!' })
  })
})

module.exports = router
